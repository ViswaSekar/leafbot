package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class CreateLead extends  Annotations{

	public CreateLead()
	{
		
	}
	
	public CreateLead enterCName()
	{
		clearAndType(locateElement("id", "createLeadForm_companyName"),"TCS");
		return this;		
	}
	
	public CreateLead enterFName()
	{
		clearAndType(locateElement("id", "createLeadForm_firstName"),"Viswa");
		return this;		
	}
	public CreateLead enterLName()
	{
		clearAndType(locateElement("id", "createLeadForm_lastName"),"Sekar");
		return this;		
	}
	public CreateLead clickCreateLead()
	{
		click(locateElement("Name", "submitButton"));
		return this;	
	}
	
	
}
