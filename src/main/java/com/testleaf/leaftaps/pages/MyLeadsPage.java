package com.testleaf.leaftaps.pages;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends  Annotations{

	public MyLeadsPage()
	{
		
	}
	
	public CreateLead clickCreateLeadButton()
	{
		click(locateElement("LinkText", "Create Lead"));
		return new CreateLead();
	}
	
}
